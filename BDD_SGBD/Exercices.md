### Entraînement sur les Schémas Relationnels

## Introduction

Nous disposons d'une base de données pour une sandwicherie. Voici les tables de cette base, comprenant des informations sur les sandwichs proposés, les clients, et les commandes passées.

---

## 1. Tables initiales

### **Table : Sandwichs**

| Nom_Sandwich  | Type          | Prix |
| ------------- | ------------- | ---- |
| Cheeseburger  | Classique     | 3.98 |
| Double cheese | Classique     | 4.90 |
| Italien       | Végétarien    | 4.90 |
| Foie gras     | Gastronomique | 8.50 |

### **Table : Clients**

| ID_Client | Nom     | Prénom | Adresse                      |
| --------- | ------- | ------ | ---------------------------- |
| 1         | Bernard | Alain  | 9, rue Bienvenu, Marseille   |
| 2         | Bernard | Yves   | 2, rue Vive la Joie, Aubagne |
| 3         | Dupont  | Sophie | 154, boulevard Aune, Lyon    |

### **Table : Commandes**

| ID_Commande | Date       | ID_Client | Nom_Sandwich | Quantité |
| ----------- | ---------- | --------- | ------------ | -------- |
| 12452       | 2019-12-11 | 1         | Italien      | 2        |
| 12453       | 2019-12-11 | 2         | Foie gras    | 1        |
| 13301       | 2019-12-23 | 3         | Cheeseburger | 3        |

---

## 2. Questions pour réflexion

1. **Analyse des clés** :
   - Quelle est la **clé primaire** de chaque table ?
   - Les tables comportent-elles des **clés étrangères** ? Si oui, lesquelles ?

2. **Problèmes de modélisation** :
   - La base est-elle bien modélisée ?
   - Proposez des améliorations pour éviter la redondance et mieux gérer les relations.

---

## 3. Modélisation d'une base pour un forum

### **Contexte** :
On souhaite modéliser une base de données simplifiée pour un **forum en ligne** avec deux tables principales :
- **Users** : contenant des informations sur les utilisateurs (pseudonyme, email, rôle).
- **Posts** : contenant des informations sur les messages (titre, contenu, date, auteur).

### **Questions** :
1. Quel serait le schéma relationnel de la table **Users** ?
2. Quel serait celui de la table **Posts** ?
3. Les tables comportent-elles des **clés primaires** ou **clés étrangères** ? Si oui, lesquelles ?
4. Comment gérer les modifications de pseudonymes des utilisateurs ?

---

## 4. Extension : Albums sur le forum

### **Nouvelle fonctionnalité** :
Chaque utilisateur peut créer un ou plusieurs **albums** contenant des messages du forum.

1. **Modèle Entité-Association** : Dessinez un modèle entité-association pour cette fonctionnalité.
2. **Schéma Relationnel** : Proposez un schéma relationnel cohérent.
3. Donnez un exemple d’enregistrement pour illustrer le fonctionnement.

---

## 5. Normalisation : Exemple pour un lycée

### **Extrait initial** :

| Nom    | Prénom | Date_Naissance | Classe | Option1 | Option2  | Option3 |
| ------ | ------ | -------------- | ------ | ------- | -------- | ------- |
| Alan   | Michel | 12/12/2005     | 2de1   | CIT     | Chinois  | NULL    |
| Bergue | John   | 13/01/2006     | 2de1   | CIT     | Chinois  | Latin   |
| Zidane | Michel | 12/12/2005     | 1S2    | Maths   | Physique | NSI     |
| Bergue | Inès   | 06/04/2004     | T-STL  | NULL    | NULL     | NULL    |

### **Problèmes** :
1. Quel est le schéma relationnel de cette base ?
2. La table contient-elle une **clé primaire** ? Des **clés étrangères** ?
3. Quels sont les défauts de conception (redondance, incohérences) ?

### **Amélioration** :
- Proposez un schéma relationnel alternatif pour corriger ces défauts.