## Le Modèle Relationnel : Fondations et Contraintes

----------------

### Le programme

![bo_relationnel](assets/bo_relationnel.png)



### Introduction : Le modèle qui a tout changé

Dans les années 1970, un chercheur d'IBM, **Edgar F. Codd**, propose un nouveau modèle pour organiser les données : le modèle relationnel. À l'époque, c'est une révolution. Les bases de données étaient alors gérées avec des structures hiérarchiques ou en réseau, complexes et rigides. Le modèle relationnel simplifie tout en utilisant des **tables** (ou relations) pour organiser les données. Résultat ? Une gestion plus efficace et un accès plus rapide aux informations. 

Aujourd'hui, des outils comme MySQL, PostgreSQL ou SQLite appliquent toujours ce modèle. Et dire qu'il a commencé par une simple publication académique… Moralité : ne sous-estimez jamais le pouvoir d'un bon papier !

---

## Les Clés : Gardiennes de l'Ordre

### Clé Primaire (Primary Key)

#### **Définition**
La clé primaire est l’attribut (ou combinaison d’attributs) qui permet d’identifier de manière unique chaque enregistrement dans une table. **C’est l’équivalent d’une carte d’identité dans une base de données.**

#### **Exemple**
Table `Etudiants` :

| ID_Etudiant | Nom    | Prénom | Age  |
| ----------- | ------ | ------ | ---- |
| 1           | Dupont | Marie  | 18   |
| 2           | Durant | Jean   | 19   |
| 3           | Morel  | Sophie | 20   |

Ici, la colonne `ID_Etudiant` est la clé primaire, car chaque valeur est unique et permet de distinguer les étudiants.

#### **Rôle crucial de la clé primaire**
- **Unicité** : Chaque enregistrement est unique.
- **Référence** : Les autres tables peuvent s’y référer via des clés étrangères.

### Clé Étrangère (Foreign Key)

#### **Définition**
Une clé étrangère est un attribut d’une table qui fait référence à la clé primaire d’une autre table. Elle établit un lien logique entre les deux tables.

#### **Exemple**
Table `Cours` :

| ID_Cours | Nom_Cours     | ID_Prof |
| -------- | ------------- | ------- |
| 101      | Mathématiques | 1       |
| 102      | Informatique  | 2       |
| 103      | Histoire      | 3       |

Table `Professeurs` :

| ID_Prof | Nom_Prof |
| ------- | -------- |
| 1       | Martin   |
| 2       | Leroy    |
| 3       | Dubois   |

Ici, la colonne `ID_Prof` de la table `Cours` est une clé étrangère qui fait référence à la colonne `ID_Prof` de la table `Professeurs`. Cela permet de savoir quel professeur enseigne quel cours.

---

## Les Contraintes d'Intégrité : Les Règles du Jeu

Les contraintes d’intégrité garantissent la cohérence des données dans une base de données relationnelle. Elles permettent d'éviter des erreurs telles que des données incohérentes ou incomplètes.

### Contrainte d'Intégrité d'Entité
**Définition** : La clé primaire ne doit jamais contenir de valeur `NULL` et doit être unique.

**Exemple** :  
Dans la table `Etudiants`, il est impossible d’avoir deux fois le même `ID_Etudiant` ou un `ID_Etudiant` vide.

| ID_Etudiant | Nom    | Prénom | Age  |
| ----------- | ------ | ------ | ---- |
| 1           | Dupont | Marie  | 18   |
| 2           | Durant | Jean   | 19   |
| 1           | Morel  | Sophie | 20   |

### Contrainte d'Intégrité Référentielle
**Définition** : Une clé étrangère doit toujours faire référence à une clé primaire existante dans la table référencée.

**Exemple** :  
Si un cours dans la table `Cours` fait référence à un `ID_Prof` inexistant dans `Professeurs`, cela crée une incohérence.

| ID_Cours | Nom_Cours     | ID_Prof |
| -------- | ------------- | ------- |
| 101      | Mathématiques | 1       |
| 102      | Informatique  | 4       |

### Contrainte d'Intégrité de Domaine
**Définition** : Les valeurs d’un attribut doivent respecter un type ou un domaine prédéfini.

**Exemple** :  
Si l’attribut `Age` est défini comme un entier positif, une valeur comme `-3` ou `Texte` serait invalide.

| ID_Etudiant | Nom    | Prénom | Age   |
| ----------- | ------ | ------ | ----- |
| 1           | Dupont | Marie  | 18    |
| 2           | Durant | Jean   | -3    |
| 3           | Morel  | Sophie | Texte |

---

## Anecdote : Le bug de la clé étrangère oubliée

Dans les années 1990, une banque a oublié d’appliquer la contrainte d’intégrité référentielle dans sa base de données. Résultat : des milliers de transactions faisaient référence à des comptes clients inexistants. Moralité : quand on parle d'argent, mieux vaut éviter les erreurs de clé étrangère !

---

## Conclusion

Le modèle relationnel repose sur des concepts simples mais puissants. Les clés primaires et étrangères permettent de lier les données de manière cohérente, tandis que les contraintes d'intégrité assurent la fiabilité des informations.

**Pousuite :** nous verrons comment utiliser le langage SQL pour manipuler ces données et appliquer ces contraintes. 

Et souvenez-vous : "Une base bien modélisée est comme une maison bien construite : elle résistera aux tempêtes (ou presque) !"

------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.